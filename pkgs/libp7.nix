{ stdenv, fetchurl, libusb, pkg-config, asciidocFull}:

stdenv.mkDerivation rec{
  version = "3.0";
  name = "libp7-${version}";

  src = fetchurl {
    url = "https://p7.planet-casio.com/pub/libp7-${version}.tar.gz";
    sha256 = "067myhfpfjnz9da10phm25w7r6cf3dbiy2dkiiw6bia0hv2b2xfc";
  };

  nativeBuildInputs = [ pkg-config asciidocFull ];
  buildInputs = [ libusb ];

  configurePhase = ''
    bash configure --prefix=$out --includedir=./includecompiled
  '';

  postInstall = ''
    mkdir $out/include
    mv ./includecompiled/*/* $out/include
  '';

  enableParallelBuilding = true;
}
