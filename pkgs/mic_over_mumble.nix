{ stdenv, fetchFromGitHub, bash }:

stdenv.mkDerivation rec {
	pname = "mic_over_mumble";
	version = "latest";

	src = fetchFromGitHub {
		owner = "pzmarzly";
		repo = pname;
		rev = "343f1b8c9500625ba04f345e11809ce0fe0147fd";
		sha256 = "sha256-gflgrsm3awqr/Iw8FA4/2wCo7y3HrZH0WlELlNczwok=";
	};

	buildPhase = ''
		substituteInPlace mic_over_mumble \
			--replace /bin/bash ${bash}/bin/bash
	'';

	installPhase = ''
		mkdir -p $out/bin
		cp mic_over_mumble $out/bin
	'';
}
