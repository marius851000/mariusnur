{ stdenv, fetchFromGitHub, writeTextFile, makeDesktopItem, allegro5, pkg-config, glew, initaddition ? ""}:

#TODO: also include image for the manual
let
  initfile = writeTextFile {
    name = "init.txt";
    text = ''
#do not manually change (should be on a write only partition).
#generated with nix
savefile ~/.liberation-circuit
  '' + initaddition;
  };

  liberation-circuit-desktop = makeDesktopItem rec {
    name = "liberation-circuit";
    exec = "liberation-circuit";
    comment = "Open source programming game";
    desktopName = "Liberation Circuit";
    categories = "Game;";
};

in
stdenv.mkDerivation rec {
  #TODO: separate help
  name = "liberation-circuit";
  version = "1.3";

  src = fetchFromGitHub {
    owner  = "linleyh";
    repo   = "liberation-circuit";
    rev    = "v${version}";
    sha256 = "028h2154vi2x0i5xc7mlzd9v5shxiysxazimxsxr5qkh8b0g82q4";
  };

  nativeBuildInputs = [ pkg-config ];
  buildInputs = [ allegro5 glew ];

  preConfigure = ''
    substituteInPlace Makefile --replace "	gcc \$(CFLAGS) -c -o \$@ \$<" "	gcc \$(CFLAGS) -c -o \$@ -Wno-format-security \$<"
    substituteInPlace src/m_main.c --replace "data/" "$out/data/" #data path is hard-coded
    substituteInPlace src/x_init.c --replace "data/" "$out/data/"
    substituteInPlace src/h_mission.c --replace "story/" "$out/story/"
    substituteInPlace src/s_mission.c --replace "proc/" "$out/proc/"
    substituteInPlace src/m_main.c --replace 'fopen("init.txt"' 'fopen("$out/init.txt"'
  '';

  installPhase = ''
    mkdir -p $out/bin
    cp bin/lc $out/bin/liberation-circuit
    cp -r bin/data bin/story bin/proc $out/
    mkdir -p $out/share/doc/liberation-circuit
    cp bin/Manual.html $out/share/doc/liberation-circuit
    cp ${initfile} $out/init.txt
    mkdir -p $out/share/applications
    cp ${liberation-circuit-desktop}/share/applications/* $out/share/applications
  '';

  enableParallelBuilding = true;

}
