{ stdenv, fetchurl, libp7, SDL, asciidocFull, pkg-config }:#libusb, pkg-config, asciidocFull}:

stdenv.mkDerivation rec{
  version = "3.0";
  name = "libp7-${version}";

  src = fetchurl {
    url = "https://p7.planet-casio.com/pub/p7utils-${version}.tar.gz";
    sha256 = "19pn5qfckgd1f6jpkgmn0c4k1qihz67gkpgkzvps428aj1lnikg7";
  };

  nativeBuildInputs = [ asciidocFull pkg-config ];
  buildInputs = [ libp7 SDL ];

  configurePhase = ''
    bash configure --prefix=$out
  '';

  enableParallelBuilding = true;
}
