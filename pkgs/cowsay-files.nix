{ stdenv, fetchFromGitHub}:

stdenv.mkDerivation rec{
  version = "bc43741b958f960c86f6563672bdbd7d890622b4";
  pname = "cowsay-files";

  src = fetchFromGitHub {
    owner  = "paulkaefer";
    repo   = "cowsay-files";
    rev    = "${version}";
    sha256 = "1jidd5fjribfribjrl9rfd08vh5k4q6f05l502ca74jandzzjrki";
  };

  installPhase = ''
    mkdir -p $out/cows
    cp cows/* $out/cows
  '';
}
