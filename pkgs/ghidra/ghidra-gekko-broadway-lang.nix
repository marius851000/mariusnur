{ stdenv, fetchFromGitHub }:

stdenv.mkDerivation rec{
	pname = "ghidra-gekko-broadway-lang-ghidra-plugin";
	version = "latest";

	src = fetchFromGitHub {
		owner = "aldelaro5";
		repo = "ghidra-gekko-broadway-lang";
		rev = "d4a57a619f3659d0fc302f5478bc06042fb69dba";
		sha256 = "0krfg5406p5jj3snbdlmnr397cmnl08n7yxkj9f95i49s26yd3nb";
	};

	installPhase = ''
		mkdir -p $out/ghidra/Ghidra/Processors/PowerPC/
		cp -r data $out/ghidra/Ghidra/Processors/PowerPC/
	'';
}
