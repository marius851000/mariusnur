{ stdenv, fetchFromGitHub, cowsay }:

stdenv.mkDerivation rec {
  name = "pokemonsay";
  version = "latest";

  src = fetchFromGitHub {
    owner  = "rominzxx";
    repo   = "pokemonsay";
    rev    = "66a1cf92115fb1124b50637fa6d7e46c9012128b";
    sha256 = "0v2mw5hnj7y8m8singy8c6f3ks3gyh7lfgp6kvc453p145gpwc5s";
  };

  buildInputs = [ cowsay ];

  patchPhase = ''
    substituteInPlace pokemonsay.sh \
      --replace cowsay ${cowsay}/bin/cowsay \
      --replace cowthink ${cowsay}/bin/cowthink \

    substituteInPlace pokemonthink.sh \
      --replace pokemonsay $out/.pokemonsay/pokemonsay.sh

  '';

  installPhase = ''
    export HOME=$out
    ./install.sh
  '';

  doInstallCheck = true;

  installCheckPhase = ''
    $out/bin/pokemonsay -p Pikachu test
    $out/bin/pokemonthink -p Pikachu test
  '';
  
  meta = with stdenv.lib; {
    description = "cowsay but with pokemon";
  };

}
