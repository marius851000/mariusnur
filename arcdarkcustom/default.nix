{ stdenv, fetchFromGitHub }:

let
srcs = {
  arc = fetchFromGitHub {
    owner  = "PapirusDevelopmentTeam";
    repo   = "arc-kde";
    rev    = "20180614";
    sha256 = "0wli16k9my7m8a9561545vjwfifmxm4w606z1h0j08msvlky40xw";
  };#TODO: directly get the src from the package

  custom = ./patch;
};
in
stdenv.mkDerivation rec {
  pname = "arcdarkcustom";
  version = "latest";

  src = srcs.arc;

  buildPhase = '''';

  installPhase = ''
    mkdir -p $out/share/themes/arcdarkcustom
    cd $out/share/themes/arcdarkcustom
    mkdir -p widgets
    cp -n ${srcs.custom}/widgets/* ${srcs.arc}/plasma/desktoptheme/Arc-Dark/widgets/* ./widgets
    mkdir -p dialogs
    cp -n ${srcs.custom}/dialogs/* ${srcs.arc}/plasma/desktoptheme/Arc-Dark/dialogs/* ./dialogs
    cp -n ${srcs.custom}/* ${srcs.arc}/plasma/desktoptheme/Arc-Dark/* ./ -r
  '';
}
