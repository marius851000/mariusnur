let
  mariusnur = import ./default.nix { };
in
mariusnur.lib.upscale_video {
  input = ./rotearth.gif; # come from wikimedia common
  name = "balle";
  extension = "webp";
  additional_encode_args = "-lossless 1";
}
