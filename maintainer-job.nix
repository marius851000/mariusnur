{ pkgs ? import <nixpkgs> {}, maintainer }:

let
  maintained = import <nixpkgs/maintainers/scripts/build.nix> { inherit maintainer; };
  #maintained = builtins.trace pkgs.hello.name [ pkgs.hello ];
in
  (builtins.listToAttrs (map (x: { name = x.name; value = x; }) maintained))
