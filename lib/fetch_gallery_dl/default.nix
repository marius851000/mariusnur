{stdenv, gallery-dl, coreutils, findutils, lib}:

{
	url ? "",
	sha256 ? "",
}:

stdenv.mkDerivation {
  name = "gallery-dl-image";
  builder = ./builder.sh;
	PATH = "${gallery-dl}/bin:${coreutils}/bin:${findutils}/bin";

	impureEnvVars = lib.fetchers.proxyImpureEnvVars;

  nativeBuildInputs = [ gallery-dl coreutils findutils ];

  outputHashAlgo = "sha256";
  outputHash = sha256;

  outputHashMode = "flat";

  inherit url;
}
