{ stdenv, lib, waifu2x-converter-cpp, ffmpeg-full }:

let
    make_6_number_string = number: lib.fixedWidthNumber 6 number;
in {
    input,
    name,
    additional_waifu_args ? "",
    additional_encode_args ? "",
    extension ? "webp",
    loop ? true
}:

let
    probe_file = stdenv.mkDerivation {
        name = "probe-data-${name}.json";

        dontUnpack = true;

        nativeBuildInputs = [ ffmpeg-full ];

        buildPhase = ''
            ffprobe -v 0 -of json -select_streams 0 -show_entries stream=r_frame_rate,nb_frames ${input} > result.json
        '';

        installPhase = ''
            mv result.json $out
        '';
    };

    video_probe = lib.head (lib.importJSON probe_file).streams;

    frames_nb = lib.toInt video_probe.nb_frames;

    frame_rate = video_probe.r_frame_rate;

    splited = stdenv.mkDerivation {
        name = "splited-${name}";

        dontUnpack = true;

        nativeBuildInputs = [ ffmpeg-full ];

        buildPhase = ''
            mkdir out
            ffmpeg -i ${input} out/%06d.png
        '';

        installPhase = ''
            mv out $out
        '';
    };

    frame_upscaled = id: stdenv.mkDerivation {
        name = "frame-${toString id}-${name}-upscaled.png";

        dontUnpack = true;

        nativeBuildInputs = [ waifu2x-converter-cpp ];

        buildPhase = ''
            waifu2x-converter-cpp \
                -i ${splited}/${make_6_number_string id}.png \
                -o out.png ${additional_waifu_args}
        '';
        
        installPhase = ''
            mv out.png $out
        '';
    };

    joined_result = stdenv.mkDerivation {
        name = "joined-${name}";

        dontUnpack = true;

        buildPhase = lib.concatStringsSep "\n" ([
            "mkdir out"
        ] ++ (map (x: "ln -s ${frame_upscaled x} out/${make_6_number_string x}.png") (lib.range 1 frames_nb)));

        installPhase = "mv out $out";
    };
in
    stdenv.mkDerivation {
        name = "${name}-upscaled.${extension}";

        dontUnpack = true;

        nativeBuildInputs = [ ffmpeg-full ];

        buildPhase = ''
            ffmpeg -framerate ${frame_rate} -pattern_type glob -i "${joined_result}/*.png" ${if loop then "-loop 0" else ""} ${additional_encode_args} result.${extension}
        '';

        installPhase = "cp result.${extension} $out";
    }