{ pkgs ? import <nixpkgs> {} }:
let
  inherit (builtins) trace;
  inherit (builtins) fetchurl;
in
rec {
  arcdarkcustom = pkgs.callPackage ./arcdarkcustom/default.nix {};

  pokemonsay = pkgs.callPackage ./pkgs/pokemonsay.nix {};

  cowsay = pkgs.callPackage ./pkgs/cowsay.nix {};

  cowsay-files = pkgs.callPackage ./pkgs/cowsay-files.nix {};

  libp7 = pkgs.callPackage ./pkgs/libp7.nix {};

  p7utils = pkgs.callPackage ./pkgs/p7utils.nix {libp7 = libp7;};

  ghidraPackage = {
    ghidra-gekko-broadway-lang = pkgs.callPackage ./pkgs/ghidra/ghidra-gekko-broadway-lang.nix {};
  };


  mic_over_mumble = pkgs.callPackage ./pkgs/mic_over_mumble.nix {};
  ## GAMES

  liberation-circuit = pkgs.callPackage ./pkgs/games/liberation-circuit.nix {};


  ## TOOLS
  lib = {
    upscale_video = pkgs.callPackage ./lib/upscale_video.nix {};

    fetchgalleryDL = pkgs.callPackage ./lib/fetch_gallery_dl {};
  };
}
